{-# LANGUAGE ApplicativeDo     #-}
{-# LANGUAGE OverloadedStrings #-}

module Unroll.Data where

import           Data.Aeson
import           Data.Aeson.Types
import           Data.Bifunctor      (first)
import           Data.Functor        ((<&>))
import           Data.Hashable       (Hashable, hashWithSalt)
import qualified Data.HashMap.Strict as HM
import           Data.Int            (Int64)
import           Data.List
import qualified Data.Text           as T
import           Data.Time.Clock     (UTCTime)
import           Data.Time.Format
import           Debug.Trace
import           Text.Read

newtype Snowflake =
  Snowflake Int64
  deriving (Eq, Show)

instance Read Snowflake where
  readsPrec i s =
    let readS = readsPrec i :: String -> [(Int64, String)]
     in map (first Snowflake) $ readS s

instance Hashable Snowflake where
  hashWithSalt h (Snowflake i) = hashWithSalt h i

instance FromJSON Snowflake where
  parseJSON = withText "Snowflake" $ either fail (pure . Snowflake) . readEither . T.unpack

data Tweet =
  Tweet
    { tweetId        :: Snowflake
    , createdAt      :: UTCTime
    , inReplyToUser  :: Maybe Snowflake
    , inReplyToTweet :: Maybe Snowflake
    , fullText       :: T.Text
    , retweeted      :: Bool
    , replies        :: [Snowflake]
    }
  deriving (Show)

instance Eq Tweet where
  (==) x y = tweetId x == tweetId y

instance FromJSON Tweet where
  parseJSON =
    withObject "Tweet" $ \obj ->
      let fullText = obj .: "full_text"
       in Tweet <$> obj .: "id_str" <*> obj `parseTwitterTime` "created_at" <*> obj .:? "in_reply_to_user_id_str" <*>
          obj .:? "in_reply_to_status_id_str" <*>
          fullText <*>
          fmap (\t -> T.take 3 t == "RT ") fullText <*>
          pure []

data Thread =
  Thread Tweet [Thread]
  deriving (Show)

parseRepliedToMaybe :: Object -> Parser (Maybe Snowflake)
parseRepliedToMaybe obj = do
  entities <- obj .: "entities"
  userMentions <- entities .: "user_mentions"
  case userMentions of
    [] -> pure Nothing
    m:_ -> do
      s <- m .: "id_str"
      pure $ readMaybe s

parseTwitterTime :: Object -> T.Text -> Parser UTCTime
parseTwitterTime obj key = parseField obj key >>= parseTimeM True defaultTimeLocale "%a %b %d %T %z %Y"

fromTweets :: [Tweet] -> HM.HashMap Snowflake Tweet
fromTweets ts = HM.fromList $ map (\t -> (tweetId t, t)) ts

isInReplyToUser :: Tweet -> Snowflake -> Bool
isInReplyToUser t u1 =
  case inReplyToUser t of
    Nothing -> False
    Just u2 -> u1 == u2

filterRetweets :: [Tweet] -> [Tweet]
filterRetweets = filter (not . retweeted)
