module Unroll.IO where

import qualified Data.ByteString.Lazy as B
import           System.IO

seekToChar :: Handle -> Char -> IO ()
seekToChar f c = do
  i <- hLookAhead f
  if i == c
    then return ()
    else do
      hGetChar f
      seekToChar f c

hGetContentsAfterChar :: Handle -> Char -> IO B.ByteString
hGetContentsAfterChar f c = do
  seekToChar f c
  hSetBinaryMode f True
  B.hGetContents f
