module Unroll.Options where

import           Data.Semigroup      ((<>))
import           Options.Applicative
import           System.Environment
import           System.IO
import           Unroll.Data         (Snowflake)

data Options =
  Options
    { tweetFile :: FilePath
    , userId    :: Snowflake
    }

options :: Parser Options
options =
  Options <$> strArgument (metavar "FILE" <> help "Twitter data download tweet.js file (use '-' for stdin)") <*>
  argument auto (metavar "USER_ID" <> help "The Twitter user ID of whoever's data dump it is")

optionsInfo :: ParserInfo Options
optionsInfo = info (helper <*> options) (failureCode 255)
