module Unroll.Exit where

import System.Environment (getProgName)
import System.Exit (exitFailure)
import System.IO

err :: String -> IO a
err s = do
  progName <- getProgName
  hPutStrLn stderr (progName ++ ": " ++ s)
  exitFailure