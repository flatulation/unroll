{-# LANGUAGE LambdaCase    #-}
{-# LANGUAGE TupleSections #-}

module Main where

import           Control.Exception
import           Data.Aeson          (eitherDecode')
import qualified Data.HashMap.Strict as HM
import           Data.List
import           Data.Maybe
import           Debug.Trace
import           Options.Applicative (execParser)
import           System.IO
import           System.IO.Error
import           Text.Pretty.Simple  (pPrint)
import           Unroll.Data
import           Unroll.IO
import           Unroll.Options

main :: IO ()
main =
  execParser optionsInfo >>= \opts -> do
    f <-
      case tweetFile opts of
        "-" -> return stdin
        fp  -> openFile fp ReadMode
    contents <-
      catchIOError
        (hGetContentsAfterChar f '[')
        (\_ -> throwIO $ userError "couldn't find the start of the JSON array in the file/input")
    case eitherDecode' contents of
      Left e -> throwIO $ userError $ "couldn't read the tweets from the input/file: " ++ e
      Right allTweets ->
        let tweets = filterRetweets allTweets
            repliesToSelf =
              mapMaybe
                (\t ->
                   case inReplyToTweet t of
                     Nothing -> Nothing
                     Just r ->
                       case inReplyToUser t of
                         Nothing -> Nothing
                         Just u ->
                           if u == userId opts
                             then Just (t, r)
                             else Nothing)
                tweets
            repliesMap =
              foldr
                (\(t, k) m ->
                   HM.alter
                     (\case
                        Nothing -> Just [t]
                        Just ts -> Just $ t : ts)
                     k
                     m)
                HM.empty
                repliesToSelf
         in print repliesToSelf
